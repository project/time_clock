# Introduction

This module works as a Digital Time clock and shows the current date & time
 as per the country timezone set in admin configuration.

For a full description of the module, visit the
[Time Clock](https://www.drupal.org/project/time_clock).


## List of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install the module as usual.


## Configuration

1. Enable the module via Drush or the admin interface.
2. Go to the admin page "Structure >> Block Layout" and
 enable the relevant block "Time Clock" in a region.
3. Set the desired values in the block configuration.
5. Now, a current date & time will be visible there.


## Maintainers

- Hemant Gupta - [hemant-gupta](https://www.drupal.org/u/hemant-gupta)
